<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\PurchasedController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth', UserController::class.'@auth')->name('auth');

Route::middleware('auth:sanctum')->group(function(){
    Route::get('products', ProductController::class.'@index')->name('products');

    Route::get('user', UserController::class.'@user')->name('user');

    Route::get('user/products', PurchasedController::class.'@index')->name('user.products');
    Route::post('user/products', PurchasedController::class.'@store')->name('user.products.store');
    Route::delete('user/products/{product_sku}', PurchasedController::class.'@destroy')->name('user.products.destroy');
});
