<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PurchasedAdditionTest extends TestCase
{
    use RefreshDatabase;

    public function testUserCanAddPurchasedProducts()
    {
        /** @var $user User */
        $user = User::factory()->create();
        $token = $user->createToken('');

        $product = Product::factory()->create();

        $response = $this->withToken($token->plainTextToken)
                         ->withHeader('Accept', 'application/json')
                         ->post(route('user.products.store'), ['product_sku' => $product->sku]);
        $response->assertSuccessful();
        $response->assertJsonStructure(['message']);
    }

    public function testUserCannotAddPurchasedProductWhichNotExist()
    {
        /** @var $user User */
        $user = User::factory()->create();
        $token = $user->createToken('');

        $response = $this->withToken($token->plainTextToken)
                         ->withHeader('Accept', 'application/json')
                         ->post(route('user.products.store'), ['product_sku' => 'bla']);
        $response->assertNotFound();
        $response->assertJsonStructure(['message']);
    }

    public function testUserCannotDeletePurchasedProductWithoutProvidingProductSku()
    {
        /** @var $user User */
        $user = User::factory()->create();
        $token = $user->createToken('');

        $response = $this->withToken($token->plainTextToken)
                         ->withHeader('Accept', 'application/json')
                         ->post(route('user.products.store'), ['product_sku' => '']);
        $response->assertNotFound();
        $response->assertJsonStructure(['message']);
    }

    public function testUserCannotAddPurchasedProductMoreThanOnce()
    {
        /** @var $user User */
        $user = User::factory()->create();
        $token = $user->createToken('');

        $product = Product::factory()->create();
        $user->products()->attach($product->id);

        $response = $this->withToken($token->plainTextToken)
                         ->withHeader('Accept', 'application/json')
                         ->post(route('user.products.store'), ['product_sku' => $product->sku]);
        $response->assertStatus(401);
        $response->assertJsonStructure(['message']);
    }

    public function testUserCannotAddPurchasedProductsIfUnAuthenticated()
    {
        $product = Product::factory()->create();

        $response = $this->withHeader('Accept', 'application/json')
                         ->post(route('user.products.store'), ['product_sku' => $product->sku]);
        $response->assertStatus(401);
        $response->assertJsonStructure(['message']);
    }
}
