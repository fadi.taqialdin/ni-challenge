<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testUserCanAuthenticateWithCorrectCredentials()
    {
        /** @var $user User */
        $user = User::factory()->create(
            [
                'password' => $password = '123456',
            ]
        );

        $response = $this->post(route('auth'), [
            'email' => $user->email,
            'password' => $password,
        ]);

        $response->assertSuccessful();
        $response->assertJsonStructure(['email', 'name', 'token']);
    }

    public function testUserCannotAuthenticateWithIncorrectPassword()
    {
        /** @var $user User */
        $user = User::factory()->create(
            [
                'password' => '123456',
            ]
        );

        $response = $this->post(route('auth'), [
            'email' => $user->email,
            'password' => 'invalid-password',
        ]);

        $response->assertStatus(400);
        $response->assertJsonStructure(['message']);
    }

    public function testUserCannotAuthenticateWithEmailThatDoesNotExist()
    {
        $response = $this->post(route('auth'), [
            'email' => 'fadi@example.com',
            'password' => 'invalid-password',
        ]);

        $response->assertStatus(400);
        $response->assertJsonStructure(['message']);
    }

    public function testUserCannotMakeMoreThanFiveAttemptsInOneMinute()
    {
        /** @var $user User */
        $user = User::factory()->create(
            [
                'password' => '123456',
            ]
        );

        foreach (range(0, 5) as $_) {
            $response = $this->post(route('auth'), [
                'email' => $user->email,
                'password' => 'invalid-password',
            ]);
        }

        $response->assertStatus(400);
        $this->assertMatchesRegularExpression(
            sprintf('/^%s$/', str_replace('\:seconds', '\d+', preg_quote(__('auth.throttle'), '/'))),
            $response->json('message')
        );
    }
}
