<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PurchasedListingTest extends TestCase
{
    use RefreshDatabase;

    public function testUserCanListPurchasedProducts()
    {
        /** @var $user User */
        $user = User::factory()->create();
        $token = $user->createToken('');

        foreach (range(0, 5) as $_) {
            $product = Product::factory()->create();
            $user->products()->attach($product->id);
        }

        $response = $this->withToken($token->plainTextToken)
                         ->withHeader('Accept', 'application/json')
                         ->get(route('user.products'));
        $response->assertSuccessful();
        $response->assertJsonStructure(['*' => ['name', 'sku']]);
    }

    public function testUserCannotListPurchasedProductsOfOthers()
    {
        /** @var $user1 User */
        $user1 = User::factory()->create();
        $token = $user1->createToken('');
        $product1 = Product::factory()->create();
        $user1->products()->attach($product1->id);

        /** @var $user2 User */
        $user2 = User::factory()->create();
        $product2 = Product::factory()->create();
        $user2->products()->attach($product2->id);

        $response = $this->withToken($token->plainTextToken)
                         ->withHeader('Accept', 'application/json')
                         ->get(route('user.products'));
        $response->assertSuccessful();
        $response->assertExactJson([['name' => $product1->name, 'sku' => $product1->sku]]);
        $response->assertJsonMissingExact([['name' => $product2->name, 'sku' => $product2->sku]]);
    }

    public function testUserCannotListPurchasedProductsIfUnAuthenticated()
    {
        $response = $this->withHeader('Accept', 'application/json')
                         ->get(route('user.products'));
        $response->assertStatus(401);
        $response->assertJsonStructure(['message']);
    }
}
