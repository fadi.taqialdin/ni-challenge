<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PurchasedDeletionTest extends TestCase
{
    use RefreshDatabase;

    public function testUserCanDeletePurchasedProducts()
    {
        /** @var $user User */
        $user = User::factory()->create();
        $token = $user->createToken('');

        $product = Product::factory()->create();
        $user->products()->attach($product->id);

        $response = $this->withToken($token->plainTextToken)
                         ->withHeader('Accept', 'application/json')
                         ->delete(route('user.products.destroy', $product->sku));
        $response->assertSuccessful();
        $response->assertJsonStructure(['message']);
    }

    public function testUserCannotDeletePurchasedProductWhichNotExist()
    {
        /** @var $user User */
        $user = User::factory()->create();
        $token = $user->createToken('');

        $response = $this->withToken($token->plainTextToken)
                         ->withHeader('Accept', 'application/json')
                         ->delete(route('user.products.destroy', 'bla'));
        $response->assertNotFound();
        $response->assertJsonStructure(['message']);
    }

    public function testUserCannotDeletePurchasedProductWithoutProvidingProductSku()
    {
        /** @var $user User */
        $user = User::factory()->create();
        $token = $user->createToken('');

        $response = $this->withToken($token->plainTextToken)
                         ->withHeader('Accept', 'application/json')
                         ->delete(route('user.products.destroy', ''));
        $response->assertStatus(405);// not allowed
        $response->assertJsonStructure(['message']);
    }

    public function testUserCannotDeletePurchasedProductNotPurchasedBefore()
    {
        /** @var $user User */
        $user = User::factory()->create();
        $token = $user->createToken('');

        $product = Product::factory()->create();

        $response = $this->withToken($token->plainTextToken)
                         ->withHeader('Accept', 'application/json')
                         ->delete(route('user.products.destroy', $product->sku));
        $response->assertStatus(401);
        $response->assertJsonStructure(['message']);
    }

    public function testUserCannotDeletePurchasedProductsIfUnAuthenticated()
    {
        $product = Product::factory()->create();

        $response = $this->withHeader('Accept', 'application/json')
                         ->delete(route('user.products.destroy', $product->sku));
        $response->assertStatus(401);
        $response->assertJsonStructure(['message']);
    }
}
