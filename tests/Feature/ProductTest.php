<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    public function testUserCanListProductsIfAuthenticated()
    {
        /** @var $user User */
        $user = User::factory()->create();
        $token = $user->createToken('');

        $response = $this->withToken($token->plainTextToken)
                         ->withHeader('Accept', 'application/json')
                         ->get(route('products'));
        $response->assertSuccessful();
        $response->assertJsonStructure(['*' => ['name', 'sku']]);
    }

    public function testUserCannotListProductsIfUnAuthenticated()
    {
        $response = $this->withHeader('Accept', 'application/json')
                         ->get(route('products'));
        $response->assertStatus(401);
        $response->assertJsonStructure(['message']);
    }
}
