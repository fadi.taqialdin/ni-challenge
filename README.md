## About

This application made to provide simple purchasing history management.

## How to run

1. Install the application dependencies using docker
```
docker-compose up
```
2. Please wait about a minute for composer installation and db migrations get ready.

3. Visit `http://127.0.0.1:8000`.

## Testing

You can run the tests using `php artisan test` command or `composer test` command.

## Documentation

You can access Swagger UI API docs from [http://127.0.0.1:8000/api/docs](http://127.0.0.1:8000/api/docs).

## Improvements

- We should provide the list of products paginated and with search ability by product name and SKU.
- We can improve the application by handling bulk addition and deletion of the purchased products.
- We should also add admin panel area with some statistics. 

