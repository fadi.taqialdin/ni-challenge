<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if(User::query()->count()){
            return;
        }

        User::query()->insert(
            [
                ['id' => 1, 'name' => 'Weston Ratke', 'email' => 'mac94@moen.com', 'password' => bcrypt('secret')],
                ['id' => 2, 'name' => 'Dr. Alberto Boyle I', 'email' => 'clark32@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 3, 'name' => 'Candelario Kassulke', 'email' => 'roselyn62@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 4, 'name' => 'Abner Mueller', 'email' => 'kabbott@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 5, 'name' => 'Mrs. Odie Miller Jr.', 'email' => 'boyer.kallie@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 6, 'name' => 'Prof. Santa Reichert', 'email' => 'aparker@dicki.net', 'password' => bcrypt('secret')],
                ['id' => 7, 'name' => 'Julio Will', 'email' => 'brooklyn.stracke@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 8, 'name' => 'Mr. Vinnie Ziemann', 'email' => 'antonietta.witting@conroy.net', 'password' => bcrypt('secret')],
                ['id' => 9, 'name' => 'Alexa Ledner', 'email' => 'pasquale.cruickshank@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 10, 'name' => 'Mrs. Janet Rolfson Sr.', 'email' => 'wrice@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 11, 'name' => 'Mrs. Meghan Mueller', 'email' => 'ozella26@waelchi.com', 'password' => bcrypt('secret')],
                ['id' => 12, 'name' => 'Dr. August Auer V', 'email' => 'reggie.gerhold@schiller.com', 'password' => bcrypt('secret')],
                ['id' => 13, 'name' => 'Dr. Lelia Hansen II', 'email' => 'gudrun.gerhold@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 14, 'name' => 'Ila Collins', 'email' => 'imogene.steuber@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 15, 'name' => 'Ms. Ashtyn Fritsch Sr.', 'email' => 'derrick35@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 16, 'name' => 'Ford Kovacek III', 'email' => 'luis.rohan@ondricka.com', 'password' => bcrypt('secret')],
                ['id' => 17, 'name' => 'Miss Kenyatta Legros IV', 'email' => 'skylar15@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 18, 'name' => 'Eleanora DuBuque', 'email' => 'kautzer.alexis@gorczany.com', 'password' => bcrypt('secret')],
                ['id' => 19, 'name' => 'Avery Boyle', 'email' => 'bheidenreich@brakus.com', 'password' => bcrypt('secret')],
                ['id' => 20, 'name' => 'Carlo Lueilwitz', 'email' => 'zackary.parker@turner.biz', 'password' => bcrypt('secret')],
                ['id' => 21, 'name' => 'Mr. Marcus Schneider', 'email' => 'llewellyn13@torphy.org', 'password' => bcrypt('secret')],
                ['id' => 22, 'name' => 'Janae Hoppe', 'email' => 'grimes.claudia@dietrich.biz', 'password' => bcrypt('secret')],
                ['id' => 23, 'name' => 'Lucy Labadie', 'email' => 'nasir.strosin@kub.com', 'password' => bcrypt('secret')],
                ['id' => 24, 'name' => 'Bill Beier', 'email' => 'dkeeling@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 25, 'name' => 'Prof. Brandy Ritchie', 'email' => 'predovic.lonie@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 26, 'name' => 'Ms. Tiffany Kuhlman MD', 'email' => 'cormier.georgianna@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 27, 'name' => 'Dr. Anna Stokes Jr.', 'email' => 'wleuschke@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 28, 'name' => 'Araceli Lind', 'email' => 'fahey.dana@krajcik.com', 'password' => bcrypt('secret')],
                ['id' => 29, 'name' => 'Prof. Damien Runolfsdottir DDS', 'email' => 'roman67@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 30, 'name' => 'Mafalda Von', 'email' => 'alfredo.lindgren@windler.com', 'password' => bcrypt('secret')],
                ['id' => 31, 'name' => 'Dr. Albert Bruen Sr.', 'email' => 'adolphus19@stanton.biz', 'password' => bcrypt('secret')],
                ['id' => 32, 'name' => 'Prof. Favian Runolfsson DVM', 'email' => 'cedrick.becker@marks.org', 'password' => bcrypt('secret')],
                ['id' => 33, 'name' => 'Gino Ledner', 'email' => 'purdy.gaylord@adams.com', 'password' => bcrypt('secret')],
                ['id' => 34, 'name' => 'Miss Britney Walker', 'email' => 'lorna65@kuhic.com', 'password' => bcrypt('secret')],
                ['id' => 35, 'name' => 'Gerardo Toy', 'email' => 'aufderhar.kasandra@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 36, 'name' => 'Mrs. Leonora Prosacco', 'email' => 'sallie.orn@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 37, 'name' => 'Alysson Herzog PhD', 'email' => 'chelsey.kiehn@gislason.com', 'password' => bcrypt('secret')],
                ['id' => 38, 'name' => 'Dr. Wilfred Leuschke', 'email' => 'gdietrich@ledner.com', 'password' => bcrypt('secret')],
                ['id' => 39, 'name' => 'Carleton Friesen', 'email' => 'rhoda86@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 40, 'name' => 'Jordy Lehner', 'email' => 'joana.botsford@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 41, 'name' => 'Carole Price', 'email' => 'hans.dibbert@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 42, 'name' => 'Ms. Albina Zulauf III', 'email' => 'bertram95@bernhard.org', 'password' => bcrypt('secret')],
                ['id' => 43, 'name' => 'Dr. Leilani Hudson III', 'email' => 'kuhic.claudine@yost.com', 'password' => bcrypt('secret')],
                ['id' => 44, 'name' => 'Andre Hessel', 'email' => 'ellsworth.kovacek@weber.biz', 'password' => bcrypt('secret')],
                ['id' => 45, 'name' => 'Alexandrea Farrell', 'email' => 'ebert.dortha@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 46, 'name' => 'Maurine Bartell', 'email' => 'okeefe.joy@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 47, 'name' => 'Laney Cummerata', 'email' => 'jacinthe.gleason@herman.com', 'password' => bcrypt('secret')],
                ['id' => 48, 'name' => 'Jamarcus Weissnat', 'email' => 'marion30@rodriguez.com', 'password' => bcrypt('secret')],
                ['id' => 49, 'name' => 'Chadd Grady', 'email' => 'dell88@jones.com', 'password' => bcrypt('secret')],
                ['id' => 50, 'name' => 'Ambrose Kunze', 'email' => 'kgutkowski@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 51, 'name' => 'Parker Goldner', 'email' => 'turner.rosie@berge.com', 'password' => bcrypt('secret')],
                ['id' => 52, 'name' => 'Jerad Murray I', 'email' => 'farrell.gaylord@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 53, 'name' => 'Mrs. Ethelyn Terry Jr.', 'email' => 'theresia92@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 54, 'name' => 'August Kemmer', 'email' => 'molly.walker@streich.com', 'password' => bcrypt('secret')],
                ['id' => 55, 'name' => 'Manuel Lakin', 'email' => 'destini90@bergnaum.info', 'password' => bcrypt('secret')],
                ['id' => 56, 'name' => 'Iva Botsford', 'email' => 'ljerde@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 57, 'name' => 'Aryanna Stracke', 'email' => 'jschaefer@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 58, 'name' => 'Bartholome Mann', 'email' => 'jevon.kessler@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 59, 'name' => 'Autumn Breitenberg', 'email' => 'blanche.rosenbaum@jacobs.com', 'password' => bcrypt('secret')],
                ['id' => 60, 'name' => 'Kevon Metz', 'email' => 'runte.sabrina@bednar.net', 'password' => bcrypt('secret')],
                ['id' => 61, 'name' => 'Rosario Breitenberg', 'email' => 'rashawn64@carroll.org', 'password' => bcrypt('secret')],
                ['id' => 62, 'name' => 'Betsy Bartoletti', 'email' => 'juvenal53@schowalter.net', 'password' => bcrypt('secret')],
                ['id' => 63, 'name' => 'Garrison Dickinson', 'email' => 'eichmann.darion@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 64, 'name' => 'Isidro Satterfield', 'email' => 'marisol03@lueilwitz.com', 'password' => bcrypt('secret')],
                ['id' => 65, 'name' => 'Jaylin Larkin Sr.', 'email' => 'gottlieb.jaron@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 66, 'name' => 'Ellsworth Marquardt', 'email' => 'kathryn.mann@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 67, 'name' => 'Jalon Bauch', 'email' => 'vlockman@von.com', 'password' => bcrypt('secret')],
                ['id' => 68, 'name' => 'Dr. Jeffry Conn Jr.', 'email' => 'blaise37@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 69, 'name' => 'Dr. Clay Kuhic', 'email' => 'helena45@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 70, 'name' => 'Michael Swift IV', 'email' => 'joelle.corkery@doyle.com', 'password' => bcrypt('secret')],
                ['id' => 71, 'name' => 'Mr. Lorenza Mohr', 'email' => 'frederick72@krajcik.com', 'password' => bcrypt('secret')],
                ['id' => 72, 'name' => 'Dr. Arnold Bruen MD', 'email' => 'kirk30@hoeger.com', 'password' => bcrypt('secret')],
                ['id' => 73, 'name' => 'Arturo Skiles V', 'email' => 'wilkinson.bernard@schimmel.org', 'password' => bcrypt('secret')],
                ['id' => 74, 'name' => 'Amira Renner Sr.', 'email' => 'denesik.guiseppe@rau.com', 'password' => bcrypt('secret')],
                ['id' => 75, 'name' => "Amanda O'Hara", 'email' => 'zwitting@goodwin.org', 'password' => bcrypt('secret')],
                ['id' => 76, 'name' => 'Phyllis Mitchell', 'email' => 'sbeatty@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 77, 'name' => 'Alejandra Fadel', 'email' => 'armand.howell@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 78, 'name' => 'Prof. Damian Wilkinson MD', 'email' => 'destany.brakus@runolfsson.com', 'password' => bcrypt('secret')],
                ['id' => 79, 'name' => 'Alexandrea Simonis', 'email' => 'alek55@shanahan.com', 'password' => bcrypt('secret')],
                ['id' => 80, 'name' => 'Maximillia Buckridge V', 'email' => 'swaniawski.elna@moen.net', 'password' => bcrypt('secret')],
                ['id' => 81, 'name' => 'Mr. Javier Miller', 'email' => 'tobin.mueller@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 82, 'name' => 'Kianna Goodwin', 'email' => 'maureen84@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 83, 'name' => 'Cyril Murphy MD', 'email' => 'luther.crona@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 84, 'name' => 'Briana Schiller', 'email' => 'gonzalo.rau@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 85, 'name' => 'Rhett Howe I', 'email' => 'ona.oreilly@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 86, 'name' => 'Mrs. Edwina Satterfield', 'email' => 'pvolkman@brakus.com', 'password' => bcrypt('secret')],
                ['id' => 87, 'name' => 'Janiya Price', 'email' => 'wyman.reese@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 88, 'name' => 'Corene Osinski', 'email' => 'robel.alaina@stanton.org', 'password' => bcrypt('secret')],
                ['id' => 89, 'name' => 'Blake Koch', 'email' => 'yschinner@yahoo.com', 'password' => bcrypt('secret')],
                ['id' => 90, 'name' => 'Mrs. Domenica Stamm IV', 'email' => 'dsawayn@ward.com', 'password' => bcrypt('secret')],
                ['id' => 91, 'name' => 'Dr. Velda Wyman', 'email' => 'kshlerin.alexys@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 92, 'name' => 'Dr. Wiley Schmeler I', 'email' => 'cartwright.jarrett@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 93, 'name' => 'Dr. Katheryn Bayer I', 'email' => 'mrau@medhurst.com', 'password' => bcrypt('secret')],
                ['id' => 94, 'name' => 'Tevin Feest', 'email' => 'bkoss@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 95, 'name' => 'Zachariah Marks', 'email' => 'swaniawski.rowland@dare.org', 'password' => bcrypt('secret')],
                ['id' => 96, 'name' => 'Brando Ledner Jr.', 'email' => 'chadd.bruen@hotmail.com', 'password' => bcrypt('secret')],
                ['id' => 97, 'name' => 'Abigail Stroman', 'email' => 'mae60@casper.com', 'password' => bcrypt('secret')],
                ['id' => 98, 'name' => 'Karen Mraz PhD', 'email' => 'rita84@christiansen.com', 'password' => bcrypt('secret')],
                ['id' => 99, 'name' => 'Mr. Osbaldo Hayes', 'email' => 'beahan.domingo@waters.org', 'password' => bcrypt('secret')],
                ['id' => 100, 'name' => 'Marta Satterfield', 'email' => 'kathryn.kuvalis@jacobs.biz', 'password' => bcrypt('secret')],
                ['id' => 101, 'name' => 'New', 'email' => 'new@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 102, 'name' => 'Maschine', 'email' => 'maschine@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 103, 'name' => 'Traktor', 'email' => 'traktor@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 104, 'name' => 'Komplete', 'email' => 'komplete@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 105, 'name' => 'Kontakt', 'email' => 'kontakt@gmail.com', 'password' => bcrypt('secret')],
                ['id' => 106, 'name' => 'Reaktor', 'email' => 'reaktor@gmail.com', 'password' => bcrypt('secret')],
            ]
        );
    }
}
