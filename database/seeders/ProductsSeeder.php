<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if(Product::query()->count()){
            return;
        }

        Product::query()->insert(
            [
                ['sku' => "battery-4", 'name' => "Battery 4"],
                ['sku' => "guitar-rig-5", 'name' => "Guitar Rig 5"],
                ['sku' => "komplete-12", 'name' => "Komplete 12"],
                ['sku' => "komplete-audio-2", 'name' => "Komplete Audio 2"],
                ['sku' => "komplete-kontrol-m32", 'name' => "Komplete Kontrol M32"],
                ['sku' => "komplete-kontrol-s49-black", 'name' => "Komplete Kontrol S49 Black"],
                ['sku' => "komplete-kontrol-s61", 'name' => "Komplete Kontrol S61"],
                ['sku' => "kontakt-6", 'name' => "Kontakt 6"],
                ['sku' => "lone-forest", 'name' => "Lone Forest"],
                ['sku' => "maschine", 'name' => "Maschine"],
                ['sku' => "maschine-jam", 'name' => "Maschine JAM"],
                ['sku' => "massive", 'name' => "Massive"],
                ['sku' => "reaktor-6", 'name' => "Reaktor 6"],
                ['sku' => "traktor-kontrol-s4", 'name' => "Traktor Kontrol S4"],
                ['sku' => "traktor-kontrol-s8", 'name' => "Traktor Kontrol S8"],
                ['sku' => "traktor-kontrol-z2", 'name' => "Traktor Kontrol Z2"],
                ['sku' => "traktor-pro-3", 'name' => "Traktor PRO 3"],
                ['sku' => "una-corda", 'name' => "Una Corda"],
            ]
        );
    }
}
