<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property string $name
 * @property string $email
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
    ];

    public function setPasswordAttribute($value) {
        if($value) {
            $this->attributes['password'] = bcrypt($value);
        }
    }

    public function products() {
        return $this->belongsToMany(Product::class, 'product_user', 'user_id', 'product_sku');
    }
}
