<?php

namespace App\Exceptions;

use App\Http\Traits\RestfulRespond;
use Exception;
use GuzzleHttp\Exception\ClientException;
use http\Exception\BadMethodCallException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use RestfulRespond;

    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        if ( $exception instanceof NotFoundHttpException ) {
            return $this->respondNotFound();
        }

        if ( $exception instanceof ModelNotFoundException ) {
            return $this->respondNotFound();
        }

        if ( $exception instanceof BadMethodCallException ) {
            return $this->respondNotFound();
        }

        if ( $exception instanceof InternalErrorException ) {
            return $this->respondInternalError();
        }

        if ( $exception instanceof ClientException ) {
            return $this->respondInternalError();
        }

        if ( $exception instanceof ValidationException ) {
            return $this->respondBadRequest($exception->validator->errors()->first());
        }

        if ( $exception instanceof MethodNotAllowedHttpException ) {
            return $this->respondMethodNotAllowed();
        }

        if ( $exception instanceof AuthorizationException ) {
            return $this->respondNotAuthorized();
        }

        if ( $exception instanceof QueryException ) {
            if($exception->errorInfo[1] == 1062) {
                return $this->respondBadRequest('Unique Constraint Violated!');
            }
        }

        if(env('APP_ENV') !== 'local'){
            return $this->respondInternalError();
        }

        return parent::render($request, $exception);
    }
}
