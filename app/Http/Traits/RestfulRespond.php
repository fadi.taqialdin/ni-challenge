<?php
namespace App\Http\Traits;

use Carbon\Carbon;
use Closure;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Response as ResponseCodes;

trait RestfulRespond {
    protected $statusCode = ResponseCodes::HTTP_OK;

    public function getLimit() {
        $limit = request('limit');
        return $limit && is_numeric($limit) && $limit > 0 && $limit <= 100 ? $limit : 10;
    }

    public function getPage() {
        $page = request('page');
        return $page && is_numeric($page) && $page > 0 ? $page : 1;
    }

    public function respondWithPagination(LengthAwarePaginator $pagination, $meta_data = []) {
        $total_pages = (int)ceil($pagination->total() / $pagination->perPage());
        $data = [
            'timestamp'  => Carbon::now()
                                  ->toDateTimeString(),
            'rooms'       => array_values($pagination->items()),
            'pagination' => [
                'total_count'  => $pagination->total(),
                'total_pages'  => $total_pages,
                'current_page' => $pagination->currentPage(),
                'next_page_url' => $pagination->currentPage() < $total_pages ?
                    url()->current() . '?page=' . ($pagination->currentPage()+1) . '&limit=' . $this->getLimit() : null,
                'previous_page_url' => $pagination->currentPage() > 1 ?
                    url()->current() . '?page=' . ($pagination->currentPage()-1) . '&limit=' . $this->getLimit() : null,
                'limit'        => (int)$pagination->perPage(),
            ],
        ];
        foreach($meta_data as $key => $value) {
            $data[$key] = $value;
        }
        return $this->respond($data);
    }

    public function respond($data, $headers = []) {
        $data = $this->filterNotNull(json_decode(json_encode($data)));
        return Response::json($data, $this->getStatusCode(), $headers, JSON_UNESCAPED_UNICODE);
    }

    private function filterNotNull($array, Closure $filterCallback = null) {
        $array = array_map(
            function($item) use ($filterCallback) {
                return is_array($item) || is_object($item) ? $this->filterNotNull((array)$item, $filterCallback) :
                    $item;
            },
            (array)$array
        );
        return array_filter(
            $array,
            $filterCallback ?? function($item) {
                return $item !== "" && $item !== null && (!is_array($item) || count($item) > 0);
            }
        );
    }

    public function getStatusCode() {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode) {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function respondBadRequest($message = 'Bad Request!') {
        return $this->setStatusCode(ResponseCodes::HTTP_BAD_REQUEST)
                    ->respondWithError($message);
    }

    public function respondMethodNotAllowed($message = 'Method Not Allowed!') {
        return $this->setStatusCode(ResponseCodes::HTTP_METHOD_NOT_ALLOWED)
                    ->respondWithError($message);
    }

    public function respondWithMessage($message) {
        return Response::json(
            [
                'message' => $message,
            ],
            $this->getStatusCode()
        );
    }

    public function respondNotFound($message = 'Not Found!') {
        return $this->setStatusCode(ResponseCodes::HTTP_NOT_FOUND)
                    ->respondWithError($message);
    }

    public function respondInternalError($message = 'Internal Error!') {
        return $this->setStatusCode(ResponseCodes::HTTP_INTERNAL_SERVER_ERROR)
                    ->respondWithError($message);
    }

    public function respondNotAuthorized($message = 'Not Authorized!') {
        return $this->setStatusCode(ResponseCodes::HTTP_UNAUTHORIZED)
                    ->respondWithError($message);
    }

    public function respondForbidden($message = 'Forbidden!') {
        return $this->setStatusCode(ResponseCodes::HTTP_FORBIDDEN)
                    ->respondWithError($message);
    }

    private function respondWithError($message) {
        return Response::json(
            [
                'message' => $message,
            ],
            $this->getStatusCode()
        );
    }
}
