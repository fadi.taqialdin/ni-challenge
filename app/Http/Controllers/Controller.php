<?php

namespace App\Http\Controllers;

use App\Http\Traits\RestfulRespond;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, RestfulRespond;

    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Native Instruments Challenge API Documentation",
     *      description="Native Instruments Challenge API Documentation",
     *      @OA\Contact(
     *          email="fadi.taqialdin@gmail.com"
     *      )
     * )
     *
     * @OA\Tag(
     *     name="User"
     * )
     * @OA\Tag(
     *     name="Products"
     * )
     * @OA\Tag(
     *     name="User Purchased Products"
     * )
     */
}
