<?php
namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class PurchasedController extends Controller {

    /**
     * @OA\Get(
     *      path="/api/user/products",
     *      summary="Get all user purchased products",
     *      tags={"User Purchased Products"},
     *      security={ {"sanctum": {} }},
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized"
     *      ),
     * )
     */
    public function index(){
        /** @var $user User */
        $user = auth()->user();

        return $this->respond($user->products);
    }

    /**
     * @OA\Post(
     *      path="/api/user/products",
     *      summary="Add user purchased product",
     *      tags={"User Purchased Products"},
     *      security={ {"sanctum": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="product_sku",
     *                  title="Product SKU",
     *                  description="SKU of an existing product",
     *                  example="battery-4"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found"
     *      ),
     * )
     */
    public function store(Request $request){
        /** @var $user User */
        $user = auth()->user();

        $product = Product::query()->where('sku', $request->product_sku)->first();

        if(!$product){
            return $this->respondNotFound('Product not found!');
        }

        if($user->products()->where('products.id', $product->id)->exists()){
            return $this->respondNotAuthorized('Product is purchased previously!');
        }

        $user->products()->attach($product->id);

        return $this->respondWithMessage($product->name . ' has added to products purchased!');
    }

    /**
     * @OA\Delete (
     *      path="/api/user/products/{product_sku}",
     *      summary="Delete user purchased product",
     *      tags={"User Purchased Products"},
     *      security={ {"sanctum": {} }},
     *      @OA\Parameter(
     *         name="product_sku",
     *         in="path",
     *         description="SKU of the product",
     *         required=true,
     *         example="battery-4",
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found"
     *      ),
     * )
     */
    public function destroy(string $product_sku){
        /** @var $user User */
        $user = auth()->user();

        $product = Product::query()->where('sku', $product_sku)->first();

        if(!$product){
            return $this->respondNotFound('Product not found!');
        }

        if( ! $user->products()->where('products.id', $product->id)->exists()){
            return $this->respondNotAuthorized('Product is not purchased previously!');
        }

        $user->products()->detach($product->id);

        return $this->respondWithMessage($product->name . ' has removed from products purchased!');
    }
}
