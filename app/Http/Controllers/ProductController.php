<?php
namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\Product;
use App\Models\User;

class ProductController extends Controller {

    /**
     * @OA\Get(
     *      path="/api/products",
     *      summary="Get all products",
     *      tags={"Products"},
     *      security={ {"sanctum": {} }},
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized"
     *      ),
     * )
     */
    public function index(){
        return $this->respond(Product::all());
    }
}
