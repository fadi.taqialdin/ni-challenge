<?php
namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;

class UserController extends Controller {

    /**
     * @OA\Post(
     *      path="/api/auth",
     *      summary="Authentication",
     *      tags={"User"},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="email",
     *                  title="Email",
     *                  description="User email",
     *                  example="mac94@moen.com"
     *              ),
     *              @OA\Property(
     *                  property="password",
     *                  title="Password",
     *                  description="User password",
     *                  example="secret"
     *              ),
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful Operation"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized"
     *      )
     * )
     */
    public function auth(LoginRequest $request){
        $request->authenticate();

        /** @var $user User */
        $user = auth()->user();

        $user->tokens()->delete();
        $token = $user->createToken('');

        return $this->respond(
            [
                'name' => $user->name,
                'email' => $user->email,
                'token' => substr($token->plainTextToken, 2)
            ]
        );
    }

    /**
     * @OA\Get(
     *      path="/api/user",
     *      summary="User info",
     *      tags={"User"},
     *      security={ {"sanctum": {} }},
     *      @OA\Response(
     *          response=200,
     *          description="Successful Operation",
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized"
     *      )
     * )
     */
    public function user(){
        /** @var $user User */
        $user = auth()->user();

        return $this->respond(
            [
                'name' => $user->name,
                'email' => $user->email
            ]
        );
    }
}
