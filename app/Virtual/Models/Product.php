<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Product",
 *     description="Product model",
 *     @OA\Xml(
 *         name="Product"
 *     )
 * )
 */
class Product
{
    /**
     * @OA\Property(
     *      title="Name",
     *      description="Name of the product",
     *      example="Nice Product"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="SKU",
     *      description="SKU of the product",
     *      example="nice-product"
     * )
     *
     * @var string
     */
    public $sku;
}
