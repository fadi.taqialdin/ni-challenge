<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="User",
 *     description="User model",
 * )
 */
class User
{
    /**
     * @OA\Property(
     *     title="Name",
     *     description="Name",
     *     example="Weston Ratke"
     * )
     *
     * @var string
     */
    private $name;
    /**
     * @OA\Property(
     *     title="Email",
     *     description="Email",
     *     format="email",
     *     example="mac94@moen.com"
     * )
     *
     * @var string
     */
    private $email;
}
